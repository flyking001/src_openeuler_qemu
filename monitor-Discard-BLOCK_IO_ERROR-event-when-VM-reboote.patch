From f5af9ac3c9af4602812060759f6f95da8725314b Mon Sep 17 00:00:00 2001
From: Yan Wang <wangyan122@huawei.com>
Date: Thu, 10 Feb 2022 11:18:13 +0800
Subject: [PATCH] monitor: Discard BLOCK_IO_ERROR event when VM rebooted

Throttled event like QAPI_EVENT_BLOCK_IO_ERROR may be queued
to limit event rate. Event may be delivered when VM is rebooted
if the event was queued in the *monitor_qapi_event_state* hash table.
Which may casue VM pause and other related problems.
Such as seabios blocked during virtio-scsi initialization:
    vring_add_buf(vq, sg, out_num, in_num, 0, 0);
    vring_kick(vp, vq, 1);
    ------------> VM paused here <-----------
    /* Wait for reply */
    while (!vring_more_used(vq)) usleep(5);

Signed-off-by: Yan Wang <wangyan122@huawei.com>
---
 include/monitor/monitor.h |  2 ++
 monitor/monitor.c         | 30 ++++++++++++++++++++++++++++++
 softmmu/runstate.c        |  1 +
 3 files changed, 33 insertions(+)

diff --git a/include/monitor/monitor.h b/include/monitor/monitor.h
index 12d395d62d..847445f972 100644
--- a/include/monitor/monitor.h
+++ b/include/monitor/monitor.h
@@ -56,4 +56,6 @@ void monitor_register_hmp(const char *name, bool info,
 void monitor_register_hmp_info_hrt(const char *name,
                                    HumanReadableText *(*handler)(Error **errp));
 
+void monitor_qapi_event_discard_io_error(void);
+
 #endif /* MONITOR_H */
diff --git a/monitor/monitor.c b/monitor/monitor.c
index 013c628695..fb4ae9531c 100644
--- a/monitor/monitor.c
+++ b/monitor/monitor.c
@@ -34,6 +34,9 @@
 #include "qemu/option.h"
 #include "sysemu/qtest.h"
 #include "trace.h"
+#include "qemu/log.h"
+#include "qapi/qmp/qjson.h"
+#include "qapi/qmp/qobject.h"
 
 /*
  * To prevent flooding clients, events can be throttled. The
@@ -767,6 +770,33 @@ int monitor_init_opts(QemuOpts *opts, Error **errp)
     return ret;
 }
 
+void monitor_qapi_event_discard_io_error(void)
+{
+    GHashTableIter event_iter;
+    MonitorQAPIEventState *evstate;
+    gpointer key, value;
+    GString *json;
+
+    qemu_mutex_lock(&monitor_lock);
+    g_hash_table_iter_init(&event_iter, monitor_qapi_event_state);
+    while (g_hash_table_iter_next(&event_iter, &key, &value)) {
+        evstate = key;
+        /* Only QAPI_EVENT_BLOCK_IO_ERROR is discarded */
+        if (evstate->event == QAPI_EVENT_BLOCK_IO_ERROR) {
+            g_hash_table_iter_remove(&event_iter);
+            json = qobject_to_json(QOBJECT(evstate->qdict));
+            qemu_log(" %s event discarded\n", json->str);
+            timer_del(evstate->timer);
+            timer_free(evstate->timer);
+            qobject_unref(evstate->data);
+            qobject_unref(evstate->qdict);
+            g_string_free(json, true);
+            g_free(evstate);
+        }
+    }
+    qemu_mutex_unlock(&monitor_lock);
+}
+
 QemuOptsList qemu_mon_opts = {
     .name = "mon",
     .implied_opt_name = "chardev",
diff --git a/softmmu/runstate.c b/softmmu/runstate.c
index 10d9b7365a..5736d908db 100644
--- a/softmmu/runstate.c
+++ b/softmmu/runstate.c
@@ -448,6 +448,7 @@ void qemu_system_reset(ShutdownCause reason)
         qapi_event_send_reset(shutdown_caused_by_guest(reason), reason);
     }
     cpu_synchronize_all_post_reset();
+    monitor_qapi_event_discard_io_error();
 }
 
 /*
-- 
2.27.0

