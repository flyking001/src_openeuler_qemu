# Whether to support Ceph rbd storage backend
%bcond_without rbd

Name: qemu
Version: 6.2.0
Release: 65
Epoch: 10
Summary: QEMU is a generic and open source machine emulator and virtualizer
License: GPLv2 and BSD and MIT and CC-BY-SA-4.0
URL: http://www.qemu.org
Source0: https://download.qemu.org/%{name}-%{version}%{?rcstr}.tar.xz
Source1: 80-kvm.rules
Source2: 99-qemu-guest-agent.rules
Source3: bridge.conf
Source4: BinDir.tar.gz

Patch0001: net-dump.c-Suppress-spurious-compiler-warning.patch
Patch0002: cpu-parse-feature-to-avoid-failure.patch
Patch0003: cpu-add-Kunpeng-920-cpu-support.patch
Patch0004: cpu-add-Cortex-A72-processor-kvm-target-support.patch
Patch0005: add-Phytium-s-CPU-models-FT-2000-and-Tengyun-S2500.patch
Patch0006: qapi-block-core-Add-retry-option-for-error-action.patch
Patch0007: block-backend-Introduce-retry-timer.patch
Patch0008: block-backend-Add-device-specific-retry-callback.patch
Patch0009: block-backend-Enable-retry-action-on-errors.patch
Patch0010: block-backend-Add-timeout-support-for-retry.patch
Patch0011: block-Add-error-retry-param-setting.patch
Patch0012: virtio_blk-Add-support-for-retry-on-errors.patch
Patch0013: vhost-cancel-migration-when-vhost-user-restarted-dur.patch
Patch0014: migration-Add-multi-thread-compress-method.patch
Patch0015: migration-Refactoring-multi-thread-compress-migratio.patch
Patch0016: migration-Add-multi-thread-compress-ops.patch
Patch0017: migration-Add-zstd-support-in-multi-thread-compressi.patch
Patch0018: migration-Add-compress_level-sanity-check.patch
Patch0019: doc-Update-multi-thread-compression-doc.patch
Patch0020: Revert-cpu-parse-feature-to-avoid-failure.patch
Patch0021: Revert-cpu-add-Cortex-A72-processor-kvm-target-suppo.patch
Patch0022: cpu-add-Cortex-A72-processor-kvm-target-support-v2.patch
Patch0023: hugepages-hugepages-files-maybe-leftover.patch
Patch0024: target-i386-Modify-the-VM-s-physical-bits-value-set-.patch
Patch0025: vfio-pci-Ascend310-need-4Bytes-quirk-in-bar4.patch
Patch0026: vfio-pci-Ascend710-need-4Bytes-quirk-in-bar0.patch
Patch0027: vfio-pci-Ascend910-need-4Bytes-quirk-in-bar0.patch
Patch0028: scsi-bus-Refactor-the-code-that-retries-requests.patch
Patch0029: scsi-disk-Add-support-for-retry-on-errors.patch
Patch0030: block-backend-Stop-retrying-when-draining.patch
Patch0031: block-Add-sanity-check-when-setting-retry-parameters.patch
Patch0032: migration-skip-cache_drop-for-bios-bootloader-and-nv.patch
Patch0033: ps2-fix-oob-in-ps2-kbd.patch
Patch0034: Currently-while-kvm-and-qemu-can-not-handle-some-kvm.patch
Patch0035: cpu-features-fix-bug-for-memory-leakage.patch
Patch0036: monitor-qmp-drop-inflight-rsp-if-qmp-client-broken.patch
Patch0037: oslib-posix-optimise-vm-startup-time-for-1G-hugepage.patch
Patch0038: nbd-server.c-fix-invalid-read-after-client-was-alrea.patch
Patch0039: qemu-nbd-make-native-as-the-default-aio-mode.patch
Patch0040: qemu-nbd-set-timeout-to-qemu-nbd-socket.patch
Patch0041: qemu-pr-fixed-ioctl-failed-for-multipath-disk.patch
Patch0042: block-enable-cache-mode-of-empty-cdrom.patch
Patch0043: block-disallow-block-jobs-when-there-is-a-BDRV_O_INA.patch
Patch0044: scsi-cdrom-Fix-crash-after-remote-cdrom-detached.patch
Patch0045: block-bugfix-disable-process-AIO-when-attach-scsi-di.patch
Patch0046: block-bugfix-Don-t-pause-vm-when-NOSPACE-EIO-happene.patch
Patch0047: scsi-bugfix-fix-division-by-zero.patch
Patch0048: i386-cache-passthrough-Update-Intel-CPUID4.EAX-25-14.patch
Patch0049: i386-cache-passthrough-Update-AMD-8000_001D.EAX-25-1.patch
Patch0050: target-arm-convert-isar-regs-to-array.patch
Patch0051: target-arm-parse-cpu-feature-related-options.patch
Patch0052: target-arm-register-CPU-features-for-property.patch
Patch0053: target-arm-Allow-ID-registers-to-synchronize-to-KVM.patch
Patch0054: target-arm-introduce-CPU-feature-dependency-mechanis.patch
Patch0055: target-arm-introduce-KVM_CAP_ARM_CPU_FEATURE.patch
Patch0056: target-arm-Add-CPU-features-to-query-cpu-model-expan.patch
Patch0057: target-arm-Add-more-CPU-features.patch
Patch0058: target-arm-ignore-evtstrm-and-cpuid-CPU-features.patch
Patch0059: target-arm-only-set-ID_PFR1_EL1.GIC-for-AArch32-gues.patch
Patch0060: target-arm-Fix-write-redundant-values-to-kvm.patch
Patch0061: target-arm-clear-EL2-and-EL3-only-when-kvm-is-not-en.patch
Patch0062: target-arm-Update-the-ID-registers-of-Kunpeng-920.patch
Patch0063: freeclock-add-qmp-command-to-get-time-offset-of-vm-i.patch
Patch0064: freeclock-set-rtc_date_diff-for-arm.patch
Patch0065: freeclock-set-rtc_date_diff-for-X86.patch
Patch0066: hw-usb-reduce-the-vpcu-cost-of-UHCI-when-VNC-disconn.patch
Patch0067: hw-net-rocker-fix-security-vulnerability.patch
Patch0068: tests-Disable-filemonitor-testcase.patch
Patch0069: seabios-convert-value-of-be16_to_cpu-to-u64-before-s.patch
Patch0070: seabios-do-not-give-back-high-ram.patch
Patch0071: seabios-drop-yield-in-smp_setup.patch
Patch0072: seabios-fix-memory-leak-when-pci-check.patch
Patch0073: seabios-increase-the-seabios-high-mem-zone-size.patch
Patch0074: seabios-increase-the-seabios-minibiostable.patch
Patch0075: IPv6-add-support-for-IPv6-protocol.patch
Patch0076: Use-post-increment-only-in-inffast.c.patch
Patch0077: util-log-add-CONFIG_DISABLE_QEMU_LOG-macro.patch
Patch0078: log-Add-some-logs-on-VM-runtime-path.patch
Patch0079: qdev-monitors-Fix-reundant-error_setg-of-qdev_add_de.patch
Patch0080: bios-tables-test-Allow-changes-to-q35-SSDT.dimmpxm-f.patch
Patch0081: smbios-Add-missing-member-of-type-4-for-smbios-3.0.patch
Patch0082: net-eepro100-validate-various-address-valuesi-CVE-20.patch
Patch0083: pci-check-bus-pointer-before-dereference.patch
Patch0084: ide-ahci-add-check-to-avoid-null-dereference-CVE-201.patch
Patch0085: tap-return-err-when-tap-TUNGETIFF-fail.patch
Patch0086: xhci-check-reg-to-avoid-OOB-read.patch
Patch0087: monitor-Discard-BLOCK_IO_ERROR-event-when-VM-reboote.patch
Patch0088: monitor-limit-io-error-qmp-event-to-at-most-once-per.patch
Patch0089: linux-headers-update-against-5.10-and-manual-clear-v.patch
Patch0090: vfio-Maintain-DMA-mapping-range-for-the-container.patch
Patch0091: vfio-migration-Add-support-for-manual-clear-vfio-dir.patch
Patch0092: update-linux-headers-Import-iommu.h.patch
Patch0093: vfio.h-and-iommu.h-header-update-against-5.10.patch
Patch0094: memory-Add-new-fields-in-IOTLBEntry.patch
Patch0095: hw-arm-smmuv3-Improve-stage1-ASID-invalidation.patch
Patch0096: hw-arm-smmu-common-Allow-domain-invalidation-for-NH_.patch
Patch0097: memory-Add-IOMMU_ATTR_VFIO_NESTED-IOMMU-memory-regio.patch
Patch0098: memory-Add-IOMMU_ATTR_MSI_TRANSLATE-IOMMU-memory-reg.patch
Patch0099: memory-Introduce-IOMMU-Memory-Region-inject_faults-A.patch
Patch0100: iommu-Introduce-generic-header.patch
Patch0101: pci-introduce-PCIPASIDOps-to-PCIDevice.patch
Patch0102: vfio-Force-nested-if-iommu-requires-it.patch
Patch0103: vfio-Introduce-hostwin_from_range-helper.patch
Patch0104: vfio-Introduce-helpers-to-DMA-map-unmap-a-RAM-sectio.patch
Patch0105: vfio-Set-up-nested-stage-mappings.patch
Patch0106: vfio-Pass-stage-1-MSI-bindings-to-the-host.patch
Patch0107: vfio-Helper-to-get-IRQ-info-including-capabilities.patch
Patch0108: vfio-pci-Register-handler-for-iommu-fault.patch
Patch0109: vfio-pci-Set-up-the-DMA-FAULT-region.patch
Patch0110: vfio-pci-Implement-the-DMA-fault-handler.patch
Patch0111: hw-arm-smmuv3-Advertise-MSI_TRANSLATE-attribute.patch
Patch0112: hw-arm-smmuv3-Store-the-PASID-table-GPA-in-the-trans.patch
Patch0113: hw-arm-smmuv3-Fill-the-IOTLBEntry-arch_id-on-NH_VA-i.patch
Patch0114: hw-arm-smmuv3-Fill-the-IOTLBEntry-leaf-field-on-NH_V.patch
Patch0115: hw-arm-smmuv3-Pass-stage-1-configurations-to-the-hos.patch
Patch0116: hw-arm-smmuv3-Implement-fault-injection.patch
Patch0117: hw-arm-smmuv3-Allow-MAP-notifiers.patch
Patch0118: pci-Add-return_page_response-pci-ops.patch
Patch0119: vfio-pci-Implement-return_page_response-page-respons.patch
Patch0120: vfio-common-Avoid-unmap-ram-section-at-vfio_listener.patch
Patch0121: vfio-Introduce-helpers-to-mark-dirty-pages-of-a-RAM-.patch
Patch0122: vfio-Add-vfio_prereg_listener_log_sync-in-nested-sta.patch
Patch0123: vfio-Add-vfio_prereg_listener_log_clear-to-re-enable.patch
Patch0124: vfio-Add-vfio_prereg_listener_global_log_start-stop-.patch
Patch0125: hw-arm-smmuv3-Post-load-stage-1-configurations-to-th.patch
Patch0126: vfio-common-Fix-incorrect-address-alignment-in-vfio_.patch
Patch0127: vfio-common-Add-address-alignment-check-in-vfio_list.patch
Patch0128: log-Add-log-at-boot-cpu-init-for-aarch64.patch
Patch0129: feature-Add-log-for-each-modules.patch
Patch0130: feature-Add-logs-for-vm-start-and-destroy.patch
Patch0131: bugfix-fix-some-illegal-memory-access-and-memory-lea.patch
Patch0132: bugfix-fix-possible-memory-leak.patch
Patch0133: bugfix-fix-eventfds-may-double-free-when-vm_id-reuse.patch
Patch0134: block-mirror-fix-file-system-went-to-read-only-after.patch
Patch0135: bugfix-fix-mmio-information-leak-and-ehci-vm-escape-.patch
Patch0136: target-i386-Fix-the-RES-memory-inc-which-caused-by-t.patch
Patch0137: virtio-scsi-bugfix-fix-qemu-crash-for-hotplug-scsi-d.patch
Patch0138: virtio-net-tap-bugfix-del-net-client-if-net_init_tap.patch
Patch0139: virtio-bugfix-clean-up-callback-when-del-virtqueue.patch
Patch0140: virtio-net-bugfix-do-not-delete-netdev-before-virtio.patch
Patch0141: virtio-net-fix-max-vring-buf-size-when-set-ring-num.patch
Patch0142: virtio-check-descriptor-numbers.patch
Patch0143: virtio-bugfix-add-rcu_read_lock-when-vring_avail_idx.patch
Patch0144: virtio-print-the-guest-virtio_net-features-that-host.patch
Patch0145: virtio-bugfix-check-the-value-of-caches-before-acces.patch
Patch0146: virtio-net-set-the-max-of-queue-size-to-4096.patch
Patch0147: virtio-net-update-the-default-and-max-of-rx-tx_queue.patch
Patch0148: vhost-user-add-unregister_savevm-when-vhost-user-cle.patch
Patch0149: qemu-img-block-dont-blk_make_zero-if-discard_zeroes-.patch
Patch0150: vhost-user-Add-support-reconnect-vhost-user-socket.patch
Patch0151: vhost-user-Set-the-acked_features-to-vm-s-featrue.patch
Patch0152: vhost-user-add-vhost_set_mem_table-when-vm-load_setu.patch
Patch0153: vhost-user-add-separate-memslot-counter-for-vhost-us.patch
Patch0154: vhost-user-quit-infinite-loop-while-used-memslots-is.patch
Patch0155: qmp-add-command-to-query-used-memslots-of-vhost-net-.patch
Patch0156: vhost-user-scsi-add-support-for-SPDK-hot-upgrade.patch
Patch0157: i6300esb-watchdog-bugfix-Add-a-runstate-transition.patch
Patch0158: bugfix-irq-Avoid-covering-object-refcount-of-qemu_ir.patch
Patch0159: seabios-add-check-to-avoid-dereference-NULL-pointer.patch
Patch0160: qemu-img-add-qemu-img-direct-create.patch
Patch0161: log-Delete-redudant-qemu_log.patch
Patch0162: bios-tables-test-Update-expected-q35-SSDT.dimmpxm-fi.patch
Patch0163: qapi-machine.json-Fix-incorrect-description-for-die-.patch
Patch0164: tests-unit-test-smp-parse-Pass-machine-type-as-argum.patch
Patch0165: tests-unit-test-smp-parse-Split-the-generic-test-in-.patch
Patch0166: tests-unit-test-smp-parse-Add-smp-with-dies-machine-.patch
Patch0167: tests-unit-test-smp-parse-Add-smp-generic-invalid-ma.patch
Patch0168: tests-unit-test-smp-parse-Add-smp-generic-valid-mach.patch
Patch0169: tests-unit-test-smp-parse-Simplify-pointer-to-compou.patch
Patch0170: tests-unit-test-smp-parse-Constify-some-pointer-stru.patch
Patch0171: hw-core-Rename-smp_parse-machine_parse_smp_config.patch
Patch0172: qemu-options-Improve-readability-of-SMP-related-Docs.patch
Patch0173: hw-core-machine-Introduce-CPU-cluster-topology-suppo.patch
Patch0174: tests-unit-test-smp-parse-Add-testcases-for-CPU-clus.patch
Patch0175: tests-unit-test-smp-parse-No-need-to-explicitly-zero.patch
Patch0176: tests-unit-test-smp-parse-Keep-default-MIN-MAX-CPUs-.patch
Patch0177: hw-arm-virt-Support-CPU-cluster-on-ARM-virt-machine.patch
Patch0178: hw-arm-virt-Support-cluster-level-in-DT-cpu-map.patch
Patch0179: hw-acpi-aml-build-Improve-scalability-of-PPTT-genera.patch
Patch0180: tests-acpi-bios-tables-test-Allow-changes-to-virt-PP.patch
Patch0181: hw-acpi-aml-build-Support-cluster-level-in-PPTT-gene.patch
Patch0182: tests-acpi-bios-table-test-Update-expected-virt-PPTT.patch
Patch0183: softmmu-device_tree-Silence-compiler-warning-with-en.patch
Patch0184: softmmu-device_tree-Remove-redundant-pointer-assignm.patch
Patch0185: hw-arm64-add-vcpu-cache-info-support.patch
Patch0186: arm64-Add-the-cpufreq-device-to-show-cpufreq-info-to.patch
Patch0187: Revert-qmp-add-command-to-query-used-memslots-of-vho.patch
Patch0188: target-arm-Fix-some-compile-errors.patch
Patch0189: pl031-support-rtc-timer-property-for-pl031.patch
Patch0190: i386-cpu-fix-compile-error-in-all-target-configure.patch
Patch0191: acpi-madt-Factor-out-the-building-of-MADT-GICC-struc.patch
Patch0192: hw-arm-virt-Assign-virt_madt_cpu_entry-to-acpi_ged-m.patch
Patch0193: arm-virt-acpi-Factor-out-CPPC-building-from-DSDT-CPU.patch
Patch0194: acpi-cpu-Prepare-build_cpus_aml-for-arm-virt.patch
Patch0195: acpi-ged-Extend-ACPI-GED-to-support-CPU-hotplug.patch
Patch0196: arm-cpu-assign-arm_get_arch_id-handler-to-get_arch_i.patch
Patch0197: tests-acpi-bios-tables-test-Allow-changes-to-virt-DS.patch
Patch0198: arm-virt-Attach-ACPI-CPU-hotplug-support-to-virt.patch
Patch0199: tests-acpi-bios-table-test-Update-expected-virt-DSDT.patch
Patch0200: arm-virt-Add-CPU-hotplug-framework.patch
Patch0201: arm-virt-Add-CPU-topology-support.patch
Patch0202: test-numa-Adjust-aarch64-numa-test.patch
Patch0203: hw-arm-virt-Factor-out-some-CPU-init-codes-to-pre_pl.patch
Patch0204: hw-arm-boot-Add-manually-register-and-trigger-of-CPU.patch
Patch0205: arm-virt-gic-Construct-irqs-connection-from-create_g.patch
Patch0206: intc-gicv3_common-Factor-out-arm_gicv3_common_cpu_re.patch
Patch0207: intc-gicv3_cpuif-Factor-out-gicv3_init_one_cpuif.patch
Patch0208: intc-kvm_gicv3-Factor-out-kvm_arm_gicv3_cpu_realize.patch
Patch0209: hw-intc-gicv3-Add-CPU-hotplug-realize-hook.patch
Patch0210: accel-kvm-Add-pre-park-vCPU-support.patch
Patch0211: intc-gicv3-Add-pre-sizing-capability-to-GICv3.patch
Patch0212: acpi-madt-Add-pre-sizing-capability-to-MADT-GICC-str.patch
Patch0213: arm-virt-Add-cpu_hotplug_enabled-field.patch
Patch0214: arm-virt-acpi-Extend-cpufreq-to-support-max_cpus.patch
Patch0215: arm-virt-Pre-sizing-MADT-GICC-GICv3-and-Pre-park-KVM.patch
Patch0216: arm-virt-Start-up-CPU-hot-plug-and-cold-plug.patch
Patch0217: pl011-reset-read-FIFO-when-UARTTIMSC-0-UARTICR-0xfff.patch
Patch0218: qcow2-fix-memory-leak-in-qcow2_read_extensions.patch
Patch0219: scsi-disk-define-props-in-scsi_block_disk-to-avoid-m.patch
Patch0220: pcie-Add-pcie-root-port-fast-plug-unplug-feature.patch
Patch0221: pcie-Compat-with-devices-which-do-not-support-Link-W.patch
Patch0222: scsi-bus-fix-unmatched-object_unref.patch
Patch0223: tools-virtiofsd-Add-rseq-syscall-to-the-seccomp-allo.patch
Patch0224: sw_64-Add-sw64-architecture-support.patch
Patch0225: coro-support-live-patch-for-libcare.patch
Patch0226: qemu-img-create-cache-paramter-only-use-for-reg-file.patch
Patch0227: scsi-bus-fix-incorrect-call-for-blk_error_retry_rese.patch
Patch0228: Revert-monitor-limit-io-error-qmp-event-to-at-most-o.patch
Patch0229: vhost-vsock-detach-the-virqueue-element-in-case-of-e.patch
Patch0230: virtio-net-fix-map-leaking-on-error-during-receive.patch
Patch0231: vfio-pci-Ascend710-change-to-bar2-quirk.patch
Patch0232: display-qxl-render-fix-race-condition-in-qxl_cursor-.patch
Patch0233: ui-cursor-fix-integer-overflow-in-cursor_alloc-CVE-2.patch
Patch0234: hw-intc-arm_gicv3-Check-for-MEMTX_OK-instead-of-MEMT.patch
Patch0235: softmmu-physmem-Simplify-flatview_write-and-address_.patch
Patch0236: softmmu-physmem-Introduce-MemTxAttrs-memory-field-an.patch
Patch0237: acpi-modify-build_ppt-del-macro-add-arm-build_pptt.patch
Patch0238: acpi-fix-QEMU-crash-when-started-with-SLIC-table.patch
Patch0239: tests-acpi-whitelist-expected-blobs-before-changing-.patch
Patch0240: tests-acpi-add-SLIC-table-test.patch
Patch0241: tests-acpi-SLIC-update-expected-blobs.patch
Patch0242: hw-block-fdc-Prevent-end-of-track-overrun-CVE-2021-3.patch
Patch0243: tests-qtest-fdc-test-Add-a-regression-test-for-CVE-2.patch
Patch0244: hw-scsi-megasas-Use-uint32_t-for-reply-queue-head-ta.patch
Patch0245: dma-Let-dma_memory_valid-take-MemTxAttrs-argument.patch
Patch0246: dma-Let-dma_memory_set-take-MemTxAttrs-argument.patch
Patch0247: dma-Let-dma_memory_rw_relaxed-take-MemTxAttrs-argume.patch
Patch0248: dma-Let-dma_memory_rw-take-MemTxAttrs-argument.patch
Patch0249: dma-Let-dma_memory_read-write-take-MemTxAttrs-argume.patch
Patch0250: dma-Let-dma_memory_map-take-MemTxAttrs-argument.patch
Patch0251: dma-Have-dma_buf_rw-take-a-void-pointer.patch
Patch0252: dma-Have-dma_buf_read-dma_buf_write-take-a-void-poin.patch
Patch0253: pci-Let-pci_dma_rw-take-MemTxAttrs-argument.patch
Patch0254: dma-Let-dma_buf_rw-take-MemTxAttrs-argument.patch
Patch0255: dma-Let-dma_buf_write-take-MemTxAttrs-argument.patch
Patch0256: dma-Let-dma_buf_read-take-MemTxAttrs-argument.patch
Patch0257: dma-Let-dma_buf_rw-propagate-MemTxResult.patch
Patch0258: dma-Let-st-_dma-take-MemTxAttrs-argument.patch
Patch0259: dma-Let-ld-_dma-take-MemTxAttrs-argument.patch
Patch0260: dma-Let-st-_dma-propagate-MemTxResult.patch
Patch0261: dma-Let-ld-_dma-propagate-MemTxResult.patch
Patch0262: pci-Let-st-_pci_dma-take-MemTxAttrs-argument.patch
Patch0263: pci-Let-ld-_pci_dma-take-MemTxAttrs-argument.patch
Patch0264: pci-Let-st-_pci_dma-propagate-MemTxResult.patch
Patch0265: pci-Let-ld-_pci_dma-propagate-MemTxResult.patch
Patch0266: hw-audio-intel-hda-Do-not-ignore-DMA-overrun-errors.patch
Patch0267: hw-audio-intel-hda-Restrict-DMA-engine-to-memories-n.patch
Patch0268: tests-qtest-intel-hda-test-Add-reproducer-for-issue-.patch
Patch0269: hw-nvme-fix-CVE-2021-3929.patch
Patch0270: acpi-validate-hotplug-selector-on-access.patch
Patch0271: virtiofsd-Drop-membership-of-all-supplementary-group.patch
Patch0272: softmmu-Always-initialize-xlat-in-address_space_tran.patch
Patch0273: numa-Enable-numa-for-SGX-EPC-sections.patch
Patch0274: numa-Support-SGX-numa-in-the-monitor-and-Libvirt-int.patch
Patch0275: doc-Add-the-SGX-numa-description.patch
Patch0276: qapi-Cleanup-SGX-related-comments-and-restore-sectio.patch
Patch0277: target-ppc-add-error-report-when-fopen-fails-in-kvmp.patch
Patch0278: target-ppc-enhance-error-report-in-kvmppc_read_int_c.patch
Patch0279: target-ppc-use-g_autofree-in-kvmppc_read_int_cpu_dt.patch
Patch0280: target-ppc-exit-1-on-failure-in-kvmppc_get_clockfreq.patch
Patch0281: bugfix-pointer-double-free-in-func-qemu_savevm_state.patch
Patch0282: vhost-user-remove-VirtQ-notifier-restore.patch
Patch0283: vhost-user-fix-VirtQ-notifier-cleanup.patch
Patch0284: nbd-allow-reconnect-on-open-with-corresponding-new-o.patch
Patch0285: block-nbd-Delete-reconnect-delay-timer-when-done.patch
Patch0286: block-nbd-Delete-open-timer-when-done.patch
Patch0287: block-nbd-Assert-there-are-no-timers-when-closed.patch
Patch0288: block-nbd-Move-s-ioc-on-AioContext-change.patch
Patch0289: hw-scsi-lsi53c895a-Do-not-abort-when-DMA-requested-a.patch
Patch0291: tests-qtest-Add-fuzz-lsi53c895a-test.patch
Patch0292: scsi-lsi53c895a-fix-use-after-free-in-lsi_do_msgout-.patch
Patch0293: scsi-lsi53c895a-really-fix-use-after-free-in-lsi_do_.patch
Patch0294: hw-usb-hcd-xhci-Fix-unbounded-loop-in-xhci_ring_chai.patch
Patch0295: net-tulip-Restrict-DMA-engine-to-memories.patch
Patch0296: exec-memory-Extract-address_space_set-from-dma_memor.patch
Patch0297: hw-elf_ops-clear-uninitialized-segment-space.patch
Patch0298: vhost-also-check-queue-state-in-the-vhost_dev_set_lo.patch
Patch0299: vhost-net-fix-improper-cleanup-in-vhost_net_start.patch
Patch0300: virtio-net-setup-vhost_dev-and-notifiers-for-cvq-onl.patch
Patch0301: job.c-add-missing-notifier-initialization.patch
Patch0302: uas-add-missing-return.patch
Patch0303: qom-assert-integer-does-not-overflow.patch
Patch0304: pci-expose-TYPE_XIO3130_DOWNSTREAM-name.patch
Patch0305: acpi-pcihp-pcie-set-power-on-cap-on-parent-slot.patch
Patch0306: hw-display-ati_2d-Fix-buffer-overflow-in-ati_2d_blt-.patch
Patch0307: ui-vnc-clipboard-fix-integer-underflow-in-vnc_client.patch
Patch0308: Remove-the-unused-local-variable-records.patch
Patch0309: Remove-this-redundant-return.patch
Patch0310: hw-vhost-user-blk-turn-on-VIRTIO_BLK_F_SIZE_MAX-feat.patch
Patch0311: migration-dirtyrate-Replace-malloc-with-g_new.patch
Patch0312: accel-kvm-kvm-all-Refactor-per-vcpu-dirty-ring-reapi.patch
Patch0313: cpus-Introduce-cpu_list_generation_id.patch
Patch0314: migration-dirtyrate-Refactor-dirty-page-rate-calcula.patch
Patch0315: softmmu-dirtylimit-Implement-vCPU-dirtyrate-calculat.patch
Patch0316: accel-kvm-kvm-all-Introduce-kvm_dirty_ring_size-func.patch
Patch0317: softmmu-dirtylimit-Implement-virtual-CPU-throttle.patch
Patch0318: softmmu-dirtylimit-Implement-dirty-page-rate-limit.patch
Patch0319: tests-Add-dirty-page-rate-limit-test.patch
Patch0320: linux-headers-include-missing-changes-from-5.17.patch
Patch0321: x86-Fix-the-64-byte-boundary-enumeration-for-extende.patch
Patch0322: x86-Add-AMX-XTILECFG-and-XTILEDATA-components.patch
Patch0323: x86-Grant-AMX-permission-for-guest.patch
Patch0324: x86-Add-XFD-faulting-bit-for-state-components.patch
Patch0325: x86-Add-AMX-CPUIDs-enumeration.patch
Patch0326: x86-add-support-for-KVM_CAP_XSAVE2-and-AMX-state-mig.patch
Patch0327: x86-Support-XFD-and-AMX-xsave-data-migration.patch
Patch0328: target-i386-kvm-do-not-access-uninitialized-variable.patch
Patch0329: KVM-x86-workaround-invalid-CPUID-0xD-9-info-on-some-.patch
Patch0330: fix-compilation-errors-of-sw64-architecture-on-x86-p.patch
Patch0331: fixed-the-error-that-no-bios-file-soft-link-was-crea.patch
Patch0332: arm-virt-Fix-vcpu-hotplug-idx_from_topo_ids.patch
Patch0333: vfio-migration-Fix-incorrect-initialization-value-fo.patch
Patch0334: hostmem-default-the-amount-of-prealloc-threads-to-sm.patch
Patch0335: Revert-vfio-common-Add-address-alignment-check-in-vf.patch
Patch0336: Revert-vfio-common-Fix-incorrect-address-alignment-i.patch
Patch0337: Revert-hw-arm-smmuv3-Post-load-stage-1-configuration.patch
Patch0338: Revert-vfio-Add-vfio_prereg_listener_global_log_star.patch
Patch0339: Revert-vfio-Add-vfio_prereg_listener_log_clear-to-re.patch
Patch0340: Revert-vfio-Add-vfio_prereg_listener_log_sync-in-nes.patch
Patch0341: Revert-vfio-Introduce-helpers-to-mark-dirty-pages-of.patch
Patch0342: Revert-vfio-common-Avoid-unmap-ram-section-at-vfio_l.patch
Patch0343: Revert-vfio-pci-Implement-return_page_response-page-.patch
Patch0344: Revert-pci-Add-return_page_response-pci-ops.patch
Patch0345: Revert-hw-arm-smmuv3-Allow-MAP-notifiers.patch
Patch0346: Revert-hw-arm-smmuv3-Implement-fault-injection.patch
Patch0347: Revert-hw-arm-smmuv3-Pass-stage-1-configurations-to-.patch
Patch0348: Revert-hw-arm-smmuv3-Fill-the-IOTLBEntry-leaf-field-.patch
Patch0349: Revert-hw-arm-smmuv3-Fill-the-IOTLBEntry-arch_id-on-.patch
Patch0350: Revert-hw-arm-smmuv3-Store-the-PASID-table-GPA-in-th.patch
Patch0351: Revert-hw-arm-smmuv3-Advertise-MSI_TRANSLATE-attribu.patch
Patch0352: Revert-vfio-pci-Implement-the-DMA-fault-handler.patch
Patch0353: Revert-vfio-pci-Set-up-the-DMA-FAULT-region.patch
Patch0354: Revert-vfio-pci-Register-handler-for-iommu-fault.patch
Patch0355: Revert-vfio-Helper-to-get-IRQ-info-including-capabil.patch
Patch0356: Revert-vfio-Pass-stage-1-MSI-bindings-to-the-host.patch
Patch0357: Revert-vfio-Set-up-nested-stage-mappings.patch
Patch0359: Revert-vfio-Introduce-helpers-to-DMA-map-unmap-a-RAM.patch
Patch0360: Revert-vfio-Introduce-hostwin_from_range-helper.patch
Patch0361: Revert-vfio-Force-nested-if-iommu-requires-it.patch
Patch0362: Revert-pci-introduce-PCIPASIDOps-to-PCIDevice.patch
Patch0363: Revert-iommu-Introduce-generic-header.patch
Patch0364: Revert-memory-Introduce-IOMMU-Memory-Region-inject_f.patch
Patch0365: Revert-memory-Add-IOMMU_ATTR_MSI_TRANSLATE-IOMMU-mem.patch
Patch0366: Revert-memory-Add-IOMMU_ATTR_VFIO_NESTED-IOMMU-memor.patch
Patch0367: Revert-hw-arm-smmu-common-Allow-domain-invalidation-.patch
Patch0368: Revert-hw-arm-smmuv3-Improve-stage1-ASID-invalidatio.patch
Patch0369: Revert-memory-Add-new-fields-in-IOTLBEntry.patch
Patch0370: Revert-vfio.h-and-iommu.h-header-update-against-5.10.patch
Patch0371: Revert-update-linux-headers-Import-iommu.h.patch
Patch0372: memory-Fix-wrong-end-address-dump.patch
Patch0373: tests-vm-use-o-IdentitiesOnly-yes-for-ssh.patch
Patch0374: linux-user-always-translate-cmsg-when-recvmsg.patch
Patch0375: target-arm-Copy-the-entire-vector-in-DO_ZIP.patch
Patch0376: hw-mem-nvdimm-fix-error-message-for-unarmed-flag.patch
Patch0377: gdb-xml-Fix-size-of-EFER-register-on-i386-architectu.patch
Patch0378: tcg-tci-fix-logic-error-when-registering-helpers-via.patch
Patch0379: virtio-mem-Don-t-skip-alignment-checks-when-warning-.patch
Patch0380: qemu-binfmt-conf.sh-fix-F-option.patch
Patch0381: Fix-several-typos-in-documentation-found-by-codespel.patch
Patch0382: hw-arm-virt-Fix-devicetree-warnings-about-the-virtio.patch
Patch0383: target-hppa-Fix-deposit-assert-from-trans_shrpw_imm.patch
Patch0384: tcg-optimize-Fix-folding-of-vector-ops.patch
Patch0385: target-arm-Add-missing-FEAT_TLBIOS-instructions.patch
Patch0386: target-riscv-pmp-fix-no-pmp-illegal-intrs.patch
Patch0387: configure-Remove-unused-python_version-variable.patch
Patch0388: configure-Remove-unused-meson_args-variable.patch
Patch0389: target-imx-reload-cmp-timer-outside-of-the-reload-pt.patch
Patch0390: configure-Add-missing-quoting-for-some-easy-cases.patch
Patch0391: hw-core-resettable-fix-reset-level-counting.patch
Patch0392: hw-usb-hcd-xhci-Reset-the-XHCIState-with-device_cold.patch
Patch0393: hw-ppc-spapr_pci.c-Use-device_cold_reset-rather-than.patch
Patch0394: hw-ide-microdrive-Use-device_cold_reset-for-self-res.patch
Patch0395: hw-i386-Use-device_cold_reset-to-reset-the-APIC.patch
Patch0396: hw-hyperv-hyperv.c-Use-device_cold_reset-instead-of-.patch
Patch0397: configure-Add-.-on-front-of-glob-of-config-devices.m.patch
Patch0398: configure-Check-mkdir-result-directly-not-via.patch
Patch0399: hw-char-pl011-fix-baud-rate-calculation.patch
Patch0400: target-i386-kvm-fix-kvmclock_current_nsec-Assertion-.patch
Patch0401: linux-headers-Update-headers-to-Linux-5.18-rc6.patch
Patch0402: virtio-get-class_id-and-pci-device-id-by-the-virtio-.patch
Patch0403: vdpa-add-vdpa-dev-support.patch
Patch0404: vdpa-add-vdpa-dev-pci-support.patch
Patch0405: vdpa-dev-mark-the-device-as-unmigratable.patch
Patch0406: docs-Add-generic-vhost-vdpa-device-documentation.patch
Patch0407: vhost-vdpa-add-memslot-getter-setter-for-vhost-vdpa.patch
Patch0408: chardev-fix-segfault-in-finalize.patch
Patch0409: tests-avocado-raspi2_initrd-Wait-for-guest-shutdown-.patch
Patch0410: Add-flex-bison-to-debian-hexagon-cross.patch
Patch0411: hw-net-can-fix-Xilinx-ZynqMP-CAN-RX-FIFO-logic.patch
Patch0412: libdecnumber-dpd-decimal64-Fix-compiler-warning-from.patch
Patch0413: Fix-several-typos-in-documentation.patch
Patch0414: tests-avocado-use-new-rootfs-for-orangepi-test.patch
Patch0415: replay-Fix-declaration-of-replay_read_next_clock.patch
Patch0416: vhost-user-Refactor-vhost-acked-features-saving.patch
Patch0417: vhost-user-Refactor-the-chr_closed_bh.patch
Patch0418: vhost-user-Fix-the-virtio-features-negotiation-flaw.patch
Patch0419: tests-qtest-libqos-e1000e-Refer-common-PCI-ID-defini.patch
Patch0420: hw-display-qxl-Have-qxl_log_command-Return-early-if-.patch
Patch0421: hw-display-qxl-Document-qxl_phys2virt.patch
Patch0422: hw-display-qxl-Pass-requested-buffer-size-to-qxl_phy.patch
Patch0423: hw-display-qxl-Avoid-buffer-overrun-in-qxl_phys2virt.patch
Patch0424: hw-display-qxl-Assert-memory-slot-fits-in-preallocat.patch
Patch0425: target-arm-Use-kvm_arm_sve_supported-in-kvm_arm_get_.patch
Patch0426: target-arm-Set-KVM_ARM_VCPU_SVE-while-probing-the-ho.patch
Patch0427: target-arm-Move-sve-probe-inside-kvm-4.15-branch.patch
Patch0428: migration-report-migration-related-thread-pid-to-lib.patch
Patch0429: migration-report-multiFd-related-thread-pid-to-libvi.patch
Patch0430: vhost_net-keep-acked_feature-only-for-NET_CLIENT_DRI.patch
Patch0431: linux-user-Add-strace-output-for-timer_settime64-sys.patch
Patch0432: fix-qemu-core-when-vhost-user-net-config-with-server.patch

BuildRequires: flex
BuildRequires: gcc
BuildRequires: make
BuildRequires: bison
BuildRequires: texinfo
BuildRequires: perl-podlators
BuildRequires: chrpath
BuildRequires: gettext
BuildRequires: python-sphinx
BuildRequires: ninja-build

BuildRequires: zlib-devel
BuildRequires: zstd-devel
BuildRequires: gtk3-devel
BuildRequires: gnutls-devel
BuildRequires: numactl-devel
BuildRequires: device-mapper-multipath-devel
BuildRequires: rdma-core-devel
BuildRequires: libcap-devel
BuildRequires: libcap-ng-devel
BuildRequires: cyrus-sasl-devel
BuildRequires: libaio-devel
BuildRequires: usbredir-devel >= 0.5.2
BuildRequires: libseccomp-devel >= 2.3.0
BuildRequires: systemd-devel
BuildRequires: libiscsi-devel
BuildRequires: snappy-devel
BuildRequires: lzo-devel
BuildRequires: ncurses-devel
BuildRequires: libattr-devel
BuildRequires: libcurl-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: pixman-devel
BuildRequires: libusbx-devel
BuildRequires: bzip2-devel
BuildRequires: libepoxy-devel
BuildRequires: libtasn1-devel
BuildRequires: libxml2-devel
BuildRequires: libudev-devel
BuildRequires: pam-devel
BuildRequires: perl-Test-Harness
BuildRequires: python3-devel
%if %{with rbd}
BuildRequires: librbd-devel
%endif
BuildRequires: krb5-devel
BuildRequires: libssh-devel
BuildRequires: glib2
BuildRequires: libfdt-devel
BuildRequires: virglrenderer-devel
BuildRequires: libslirp-devel
BuildRequires: liburing-devel

# for upgrade from qemu-kvm
Provides: qemu-kvm
Obsoletes: qemu-kvm < 10:6.2.0

Requires(post): /usr/bin/getent
Requires(post): /usr/sbin/groupadd
Requires(post): /usr/sbin/useradd
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(postun): qemu-block-iscsi
Requires(postun): qemu-block-curl
Requires(postun): qemu-hw-usb-host
Requires: libgcc
Requires: liburing


%description
QEMU is a FAST! processor emulator using dynamic translation to achieve good emulation speed.

QEMU has two operating modes:

   Full system emulation. In this mode, QEMU emulates a full system (for example a PC),
   including one or several processors and various peripherals. It can be used to launch
   different Operating Systems without rebooting the PC or to debug system code.

   User mode emulation. In this mode, QEMU can launch processes compiled for one CPU on another CPU.
   It can be used to launch the Wine Windows API emulator (https://www.winehq.org) or to ease
   cross-compilation and cross-debugging.
You can refer to https://www.qemu.org for more infortmation.

%package guest-agent
Summary: QEMU guest agent
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
%description guest-agent
This package provides an agent to run inside guests, which communicates
with the host over a virtio-serial channel named "org.qemu.guest_agent.0"
Please refer to https://wiki.qemu.org/Features/GuestAgent for more information.

%package help
Summary: Documents for qemu
Buildarch: noarch
%description help
This package provides documents for qemu related man help and information.

%package  img
Summary: QEMU command line tool for manipulating disk images
%description img
This package provides a command line tool for manipulating disk images

%if %{with rbd}
%package  block-rbd
Summary: Qemu-block-rbd
%description block-rbd
This package provides RBD support for Qemu
%endif

%package  block-ssh
Summary: Qemu-block-ssh
%description block-ssh
This package provides block-ssh support for Qemu

%package  block-iscsi
Summary: Qemu-block-iscsi
%description block-iscsi
This package provides block-iscsi support for Qemu

%package  block-curl
Summary: Qemu-block-curl
%description block-curl
This package provides block-curl support for Qemu

%package hw-usb-host
Summary: Qemu-hw-usb-host
%description hw-usb-host
This package provides hw-usb-host support for Qemu

%ifarch %{ix86} x86_64
%package seabios
Summary: QEMU seabios
%description seabios
This package include bios-256k.bin and bios.bin of seabios
%endif

%package system-aarch64
Summary: Qemu-system-aarch64
Requires: qemu
%description system-aarch64
This package provides the QEMU system emulator for AArch64.

%package system-arm
Summary: Qemu-system-arm
Requires: qemu
%description system-arm
This package provides the QEMU system emulator for ARM.

%package system-x86_64
Summary: Qemu-system-x86_64
Requires: qemu
%description system-x86_64
This package provides the QEMU system emulator for x86_64.

%package system-riscv
Summary: Qemu-system-riscv32, Qemu-system-riscv64
Requires: qemu
%description system-riscv
This package provides the QEMU system emulator for riscv.

%prep
%setup -q -n qemu-%{version}%{?rcstr}
%autopatch -p1

%build
%ifarch x86_64
buildarch="x86_64-softmmu"
targetarch="aarch64-softmmu arm-softmmu riscv32-softmmu riscv64-softmmu"
%endif
%ifarch aarch64
buildarch="aarch64-softmmu"
targetarch="x86_64-softmmu arm-softmmu riscv32-softmmu riscv64-softmmu"
%endif

buildldflags="VL_LDFLAGS=-Wl,--build-id"
qemubuilddir="build"

tar xf %{SOURCE4}
cd BinDir/
\cp -r -a * ../
cd ../


./configure \
    --prefix=%{_prefix}   \
    --target-list="${buildarch} ${targetarch}"     \
    --extra-cflags="%{optflags} -fPIE -DPIE -fPIC -ftls-model=initial-exec"    \
    --extra-ldflags="-Wl,--build-id -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack" \
    --datadir=%{_datadir} \
    --docdir=%{_docdir}/ \
    --libdir=%{_libdir}     \
    --libexecdir=%{_libexecdir} \
    --localstatedir=%{_localstatedir} \
    --sysconfdir=%{_sysconfdir} \
    --interp-prefix=%{_prefix}/qemu-%%M \
    --firmwarepath=%{_datadir}/%{name}    \
    --with-pkgversion=%{name}-%{version}-%{release} \
    --python=/usr/bin/python3 \
    --enable-slirp=system \
    --enable-slirp-smbd \
    --enable-gtk \
    --enable-docs \
    --enable-guest-agent \
    --enable-pie \
    --enable-numa \
    --enable-mpath \
    --disable-libnfs \
    --disable-bzip2 \
    --enable-kvm \
    --enable-tcg \
    --enable-rdma \
    --enable-linux-aio \
    --enable-linux-io-uring \
    --enable-cap-ng \
    --enable-vhost-user \
    --enable-vhost-net \
    --enable-vhost-kernel \
    --enable-vhost-user-blk-server \
    --enable-vhost-vdpa \
    --enable-vhost-vsock \
    --enable-tpm \
    --enable-modules \
    --enable-libssh \
    --enable-fdt \
    --enable-virglrenderer \
    --enable-cap-ng \
    --enable-libusb \
%if %{with rbd}
    --enable-rbd \
%else
    --disable-rbd \
%endif
    --disable-dmg \
    --disable-qcow1 \
    --disable-vdi \
    --disable-vvfat \
    --disable-qed \
    --disable-parallels \
    --disable-capstone \
    --disable-smartcard \
    --enable-zstd \
    --disable-brlapi \
    --disable-plugins \
    --enable-debug

make %{?_smp_mflags} $buildldflags V=1

cp ${qemubuilddir}/${buildarch}/qemu-system-* qemu-kvm

%install

make %{?_smp_mflags} DESTDIR=%{buildroot} \
    install

%find_lang %{name}
install -m 0755 qemu-kvm  %{buildroot}%{_libexecdir}/
ln -s  %{_libexecdir}/qemu-kvm %{buildroot}/%{_bindir}/qemu-kvm

install -D -p -m 0644 contrib/systemd/qemu-pr-helper.service %{buildroot}%{_unitdir}/qemu-pr-helper.service
install -D -p -m 0644 contrib/systemd/qemu-pr-helper.socket %{buildroot}%{_unitdir}/qemu-pr-helper.socket
install -D -p -m 0644 qemu.sasl %{buildroot}%{_sysconfdir}/sasl2/qemu.conf
install -D -m 0644 %{_sourcedir}/bridge.conf %{buildroot}%{_sysconfdir}/qemu/bridge.conf
install -D -m 0644 %{_sourcedir}/80-kvm.rules %{buildroot}/usr/lib/udev/rules.d/80-kvm.rules

# For qemu-guest-agent package
%global _udevdir /lib/udev/rules.d
install -D -p -m 0644 contrib/systemd/qemu-guest-agent.service %{buildroot}%{_unitdir}/qemu-guest-agent.service
install -D -m 0644 %{_sourcedir}/99-qemu-guest-agent.rules %{buildroot}%{_udevdir}/99-qemu-guest-agent.rules
mkdir -p %{buildroot}%{_localstatedir}/log
touch %{buildroot}%{_localstatedir}/log/qga-fsfreeze-hook.log

# For qemu docs package
%global qemudocdir %{_docdir}/%{name}
rm -rf %{buildroot}%{qemudocdir}/specs
rm -rf %{buildroot}%{qemudocdir}/.buildinfo
rm -rf %{buildroot}%{qemudocdir}/objects.inv
rm -rf %{buildroot}%{qemudocdir}/genindex.html
rm -rf %{buildroot}%{qemudocdir}/index.html
install -D -p -m 0644 -t %{buildroot}%{qemudocdir} README.rst COPYING COPYING.LIB LICENSE
chmod -x %{buildroot}%{_mandir}/man1/*

rm -rf %{buildroot}%{_datadir}/%{name}/vgabios-ati.bin
rm -rf %{buildroot}%{_datadir}/%{name}/bios-microvm.bin
rm -rf %{buildroot}%{_datadir}/%{name}/multiboot_dma.bin
rm -rf %{buildroot}%{_datadir}/%{name}/openbios-*
rm -rf %{buildroot}%{_datadir}/%{name}/slof.bin
rm -rf %{buildroot}%{_datadir}/%{name}/QEMU,*.bin
rm -rf %{buildroot}%{_datadir}/%{name}/bamboo.dtb
rm -rf %{buildroot}%{_datadir}/%{name}/canyonlands.dtb
rm -rf %{buildroot}%{_datadir}/%{name}/hppa-firmware.img
rm -rf %{buildroot}%{_datadir}/%{name}/palcode-clipper
rm -rf %{buildroot}%{_datadir}/%{name}/petalogix-*
rm -rf %{buildroot}%{_datadir}/%{name}/ppc_*
rm -rf %{buildroot}%{_datadir}/%{name}/qemu_vga.ndrv
rm -rf %{buildroot}%{_datadir}/%{name}/s390-*
rm -rf %{buildroot}%{_datadir}/%{name}/skiboot.lid
rm -rf %{buildroot}%{_datadir}/%{name}/spapr-*
rm -rf %{buildroot}%{_datadir}/%{name}/u-boot*
rm -rf %{buildroot}%{_bindir}/ivshmem*
rm -f %{buildroot}%{_datadir}/%{name}/edk2*
rm -rf %{buildroot}%{_datadir}/%{name}/firmware
rm -rf %{buildroot}%{_datadir}/%{name}/qemu-nsis.bmp
rm -rf %{buildroot}%{_libdir}/%{name}/audio-oss.so
rm -rf %{buildroot}%{_libdir}/%{name}/audio-pa.so
rm -rf %{buildroot}%{_libdir}/%{name}/block-gluster.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-curses.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-gtk.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-sdl.so
rm -rf %{buildroot}%{_libdir}/%{name}/audio-spice.so
rm -rf %{buildroot}%{_libdir}/%{name}/chardev-baum.so
rm -rf %{buildroot}%{_libdir}/%{name}/chardev-spice.so
rm -rf %{buildroot}%{_libdir}/%{name}/hw-display-qxl.so
rm -rf %{buildroot}%{_libdir}/%{name}/hw-s390x-virtio-gpu-ccw.so
rm -rf %{buildroot}%{_libdir}/%{name}/hw-usb-redirect.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-opengl.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-spice-app.so
rm -rf %{buildroot}%{_libdir}/%{name}/ui-spice-core.so

rm -rf %{buildroot}%{_libexecdir}/vhost-user-gpu
rm -rf %{buildroot}%{_datadir}/%{name}/vhost-user/50-qemu-gpu.json
rm -rf %{buildroot}%{_datadir}/%{name}/vhost-user/50-qemu-virtiofsd.json

%if %{with rbd}
strip %{buildroot}%{_libdir}/%{name}/block-rbd.so
%endif
strip %{buildroot}%{_libdir}/%{name}/block-iscsi.so
strip %{buildroot}%{_libdir}/%{name}/block-curl.so
strip %{buildroot}%{_libdir}/%{name}/block-ssh.so
strip %{buildroot}%{_libdir}/%{name}/hw-usb-host.so

for f in %{buildroot}%{_bindir}/* %{buildroot}%{_libdir}/* \
         %{buildroot}%{_libexecdir}/*; do
  if file $f | grep -q ELF | grep -q -i shared; then chrpath --delete $f; fi
done

%check
echo "#define CONFIG_DISABLE_QEMU_LOG" >> build/config-host.h
make %{?_smp_mflags} $buildldflags V=1
make check V=1 %{?_smp_mflags}

%pre
getent group kvm >/dev/null || groupadd -g 36 -r kvm
getent group qemu >/dev/null || groupadd -g 107 -r qemu
getent passwd qemu >/dev/null || \
  useradd -r -u 107 -g qemu -G kvm -d / -s /sbin/nologin \
    -c "qemu user" qemu

%post guest-agent
%systemd_post qemu-guest-agent.service
%preun guest-agent
%systemd_preun qemu-guest-agent.service
%postun guest-agent
%systemd_postun_with_restart qemu-guest-agent.service

%files  -f %{name}.lang
%dir %{_datadir}/%{name}/
%{_libexecdir}/qemu-kvm
%{_bindir}/qemu-kvm
%{_libdir}/%{name}/accel-qtest-*.so
%ifarch x86_64
%{_libdir}/%{name}/accel-tcg-*.so
%{_libdir}/%{name}/hw-display-virtio-vga-gl.so
%{_libdir}/%{name}/hw-display-virtio-vga.so
%endif
%{_libdir}/%{name}/hw-display-virtio-gpu-gl.so
%{_libdir}/%{name}/hw-display-virtio-gpu-pci-gl.so
%{_libdir}/%{name}/hw-display-virtio-gpu-pci.so
%{_libdir}/%{name}/hw-display-virtio-gpu.so
%{_datadir}/%{name}/efi-virtio.rom
%{_datadir}/%{name}/efi-e1000.rom
%{_datadir}/%{name}/efi-e1000e.rom
%{_datadir}/%{name}/efi-rtl8139.rom
%{_datadir}/%{name}/efi-pcnet.rom
%{_datadir}/%{name}/efi-ne2k_pci.rom
%{_datadir}/%{name}/efi-eepro100.rom
%{_datadir}/%{name}/efi-vmxnet3.rom
%{_datadir}/%{name}/pxe-virtio.rom
%{_datadir}/%{name}/pxe-e1000.rom
%{_datadir}/%{name}/pxe-ne2k_pci.rom
%{_datadir}/%{name}/pxe-pcnet.rom
%{_datadir}/%{name}/pxe-rtl8139.rom
%{_datadir}/%{name}/pxe-eepro100.rom
%{_datadir}/%{name}/qboot.rom
%{_datadir}/%{name}/trace-events-all
%{_datadir}/applications/qemu.desktop
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/%{name}/keymaps/
%{_bindir}/elf2dmp
%{_bindir}/qemu-edid
%{_bindir}/qemu-keymap
%{_bindir}/qemu-pr-helper
%{_libexecdir}/virtfs-proxy-helper
%{_libexecdir}/virtiofsd
%{_unitdir}/qemu-pr-helper.service
%{_unitdir}/qemu-pr-helper.socket
%attr(4755, root, root) %{_libexecdir}/qemu-bridge-helper
%config(noreplace) %{_sysconfdir}/sasl2/qemu.conf
%dir %{_sysconfdir}/qemu
%config(noreplace) %{_sysconfdir}/qemu/bridge.conf
/usr/lib/udev/rules.d/80-kvm.rules
%doc %{qemudocdir}/COPYING
%doc %{qemudocdir}/COPYING.LIB
%doc %{qemudocdir}/LICENSE
%ifarch x86_64
%{_datadir}/%{name}/bios.bin
%{_datadir}/%{name}/bios-256k.bin
%{_datadir}/%{name}/vgabios.bin
%{_datadir}/%{name}/vgabios-cirrus.bin
%{_datadir}/%{name}/vgabios-qxl.bin
%{_datadir}/%{name}/vgabios-stdvga.bin
%{_datadir}/%{name}/vgabios-vmware.bin
%{_datadir}/%{name}/vgabios-virtio.bin
%{_datadir}/%{name}/vgabios-ramfb.bin
%{_datadir}/%{name}/vgabios-bochs-display.bin
%{_datadir}/%{name}/linuxboot.bin
%{_datadir}/%{name}/linuxboot_dma.bin
%{_datadir}/%{name}/pvh.bin
%{_datadir}/%{name}/multiboot.bin
%{_datadir}/%{name}/kvmvapic.bin
%{_datadir}/%{name}/sgabios.bin
%endif
%exclude %{_datadir}/%{name}/core3-hmcode
%exclude %{_datadir}/%{name}/core3-reset
%exclude %{_datadir}/%{name}/uefi-bios-sw

%files system-aarch64
%{_bindir}/qemu-system-aarch64

%files system-arm
%{_bindir}/qemu-system-arm
%{_datadir}/%{name}/npcm7xx_bootrom.bin

%files system-x86_64
%{_bindir}/qemu-system-x86_64
%ifnarch x86_64
%{_libdir}/%{name}/accel-tcg-*.so
%{_libdir}/%{name}/hw-display-virtio-vga-gl.so
%{_libdir}/%{name}/hw-display-virtio-vga.so
%{_datadir}/%{name}/bios.bin
%{_datadir}/%{name}/bios-256k.bin
%{_datadir}/%{name}/vgabios.bin
%{_datadir}/%{name}/vgabios-cirrus.bin
%{_datadir}/%{name}/vgabios-qxl.bin
%{_datadir}/%{name}/vgabios-stdvga.bin
%{_datadir}/%{name}/vgabios-vmware.bin
%{_datadir}/%{name}/vgabios-virtio.bin
%{_datadir}/%{name}/vgabios-ramfb.bin
%{_datadir}/%{name}/vgabios-bochs-display.bin
%{_datadir}/%{name}/linuxboot.bin
%{_datadir}/%{name}/linuxboot_dma.bin
%{_datadir}/%{name}/pvh.bin
%{_datadir}/%{name}/multiboot.bin
%{_datadir}/%{name}/kvmvapic.bin
%{_datadir}/%{name}/sgabios.bin
%endif

%files system-riscv
%{_bindir}/qemu-system-riscv32
%{_bindir}/qemu-system-riscv64
%{_datadir}/%{name}/opensbi-riscv*.bin
%{_datadir}/%{name}/opensbi-riscv*.elf

%files help
%dir %{qemudocdir}
%doc %{qemudocdir}/about
%doc %{qemudocdir}/devel
%doc %{qemudocdir}/interop
%doc %{qemudocdir}/search*
%doc %{qemudocdir}/_static
%doc %{qemudocdir}/system
%doc %{qemudocdir}/tools
%doc %{qemudocdir}/user
%doc %{qemudocdir}/README.rst
%{_mandir}/man1/qemu.1*
%{_mandir}/man1/qemu-img.1*
%{_mandir}/man1/qemu-storage-daemon.1*
%{_mandir}/man1/virtfs-proxy-helper.1*
%{_mandir}/man1/virtiofsd.1*
%{_mandir}/man7/qemu-block-drivers.7*
%{_mandir}/man7/qemu-cpu-models.7*
%{_mandir}/man7/qemu-ga-ref.7*
%{_mandir}/man7/qemu-qmp-ref.7*
%{_mandir}/man7/qemu-storage-daemon-qmp-ref.7*
%{_mandir}/man8/qemu-ga.8*
%{_mandir}/man8/qemu-nbd.8*
%{_mandir}/man8/qemu-pr-helper.8*


%files guest-agent
%defattr(-,root,root,-)
%{_bindir}/qemu-ga
%{_mandir}/man8/qemu-ga.8*
%{_unitdir}/qemu-guest-agent.service
%{_udevdir}/99-qemu-guest-agent.rules
%ghost %{_localstatedir}/log/qga-fsfreeze-hook.log

%files img
%{_bindir}/qemu-img
%{_bindir}/qemu-io
%{_bindir}/qemu-nbd
%{_bindir}/qemu-storage-daemon

%if %{with rbd}
%files block-rbd
%{_libdir}/%{name}/block-rbd.so
%endif

%files block-ssh
%{_libdir}/%{name}/block-ssh.so

%files block-iscsi
%{_libdir}/%{name}/block-iscsi.so

%files block-curl
%{_libdir}/%{name}/block-curl.so

%files hw-usb-host
%{_libdir}/%{name}/hw-usb-host.so

%ifarch %{ix86} x86_64
%files seabios
%{_datadir}/%{name}/bios-256k.bin
%{_datadir}/%{name}/bios.bin
%endif

%changelog
* Tue Dec 20 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-65
- linux-user: Add strace output for timer_settime64() syscall
- fix qemu-core when vhost-user-net config with server mode

* Wed Dec 14 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-64
- target/arm: Fix kvm probe of ID_AA64ZFR0
- migration: report migration/multiFd related thread pid to libvirt
- vhost_net: keep acked_feature only for NET_CLIENT_DRIVER_VHOST_USER

* Mon Dec 12 2022 Qiang Wei <qiang.wei@suse.com> - 10:6.2.0-63
- Use bcond_without to control conditional build

* Thu Dec 8 2022 Qiang Wei <qiang.wei@suse.com> - 10:6.2.0-62
- Make Ceph rbd support an optional feature.

* Wed Dec 07 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-61
- BuildRequires add make

* Tue Dec 06 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-60
- sync some bugfix patches from upstream
- fix the virtio features negotiation flaw
- fix CVE-2022-4144

* Tue Nov 22 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-59
- arm/virt: Fix vcpu hotplug idx_from_topo_ids
- Revert patches related to the vSVA
- sync some bugfix patches from upstream
- add generic vDPA device support

* Mon Nov 14 2022 weishaokun <weishaokun@kylinos.cn> - 10:6.2.0-58
- support io-uring by adding --enable-io-uring compilation option

* Tue Nov 08 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-57
- build: make check with -j

* Mon Nov 07 2022 yuelongguang <yuelg@chinaunicom.cn> - 10:6.2.0-56
- support rbd by adding --enable-rbd compilation option

* Thu Nov 03 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-55
- support dirty restraint on vCPU
- support SPR AMX in Qemu
- fix compilation errors of sw64

* Mon Oct 24 2022 fushanqing <fushanqing@kylinos.cn> - 10:6.2.0-54
- add '--enable-slirp' compilation options

* Fri Oct 21 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-53
- ui/vnc-clipboard: fix integer underflow in vnc_client_cut_text_ext (CVE-2022-3165)

* Fri Sep 30 2022 wanbo <wanbo13@huawei.com> - 10:6.2.0-52
- job.c: add missing notifier initialization
- uas: add missing return
- qom: assert integer does not overflow
- pci: expose TYPE_XI03130_DOWNSTREAM name
- acpi: pcihp: pcie: set power on cap on parent slot
- hw/display/ati_2d: Fix buffer overflow in ati_2d_blt

* Fri Sep 30 2022 zhangxinhao <zhangxinhao1@huawei.com> - 10:6.2.0-51
- exec/memory: Extract address_space_set() from dma_memory_set()
- hw/elf_ops: clear uninitialized segment space
- vhost: also check queue state in the vhost_dev_set_log
- vhost-net: fix improper cleanup in vhost_net_start
- virtio-net: setup vhost_dev and notifiers for cvq only

* Fri Sep 30 2022 zhangbo <oscar.zhangbo@huawei.com> - 10:6.2.0-50
- net: tulip: Restrict DMA engine to memories (CVE-2020-14394)

* Sat Sep 03 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-49
- hw/usb/hcd-xhci: Fix unbounded loop in xhci_ring_chain_length() (CVE-2020-14394)

* Tue Aug 30 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-48
- hw/scsi/lsi53c895a: Do not abort when DMA requested and no data queued
- tests/qtest: Add fuzz-lsi53c895a-test
- scsi/lsi53c895a: fix use-after-free in lsi_do_msgout (CVE-2022-0216)
- scsi/lsi53c895a: really fix use-after-free in lsi_do_msgout (CVE-2022-0216)

* Mon Aug 29 2022 Zhang Bo <oscar.zhangbo@huawei.com> - 10:6.2.0-47
- backport nbd related patches to avoid vm crash during migration

* Thu Aug 25 2022 yezengruan <yezengruan@huawei.com> - 10:6.2.0-46
- vhost-user: remove VirtQ notifier restore
- vhost-user: fix VirtQ notifier cleanup
- enable vDPA build params
- Provides qemu-kvm for upgrade

* Thu Aug 11 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-45
- numa: Enable numa for SGX EPC sections
- target/ppc: enhance error handling in kvmppc_read_int*
- fix pointer double free in func qemu_savevm_state_complete_precopy_non_iterable

* Mon Jul 25 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-44
- add Requires libgcc

* Tue Jul 19 2022 cenhuilin <cenhuilin@kylinos.cn> - 2:6.2.0-43
- softmmu Always initialize xlat in address_space_tran (CVE-2022-35414)

* Tue Jul 12 2022 liuxiangdong <liuxiangdong5@huawei.com> - 2:6.2.0-42
- acpi: validate hotplug selector on access
- virtiofsd: Drop membership of all supplementary groups (CVE-2022-0358)

* Wed Jun 22 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-41
- hw/nvme: fix CVE-2021-3929

* Mon Jun 20 2022 zhangziyang <zhangziyang1@huawei.com> - 2:6.2.0-40
- add qemu-system-riscv rpm package build

* Thu Jun 09 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-39
- hw/scsi/megasas: Use uint32_t for reply queue head/tail values
- dma: Let dma_memory_valid() take MemTxAttrs argument
- dma: Let dma_memory_set() take MemTxAttrs argument
- dma: Let dma_memory_rw_relaxed() take MemTxAttrs argument
- dma: Let dma_memory_rw() take MemTxAttrs argument
- dma: Let dma_memory_read/write() take MemTxAttrs argument
- dma: Let dma_memory_map() take MemTxAttrs argument
- dma: Have dma_buf_rw() take a void pointer
- dma: Have dma_buf_read() / dma_buf_write() take a void pointer
- pci: Let pci_dma_rw() take MemTxAttrs argument
- dma: Let dma_buf_rw() take MemTxAttrs argument
- dma: Let dma_buf_write() take MemTxAttrs argument
- dma: Let dma_buf_read() take MemTxAttrs argument
- dma: Let dma_buf_rw() propagate MemTxResult
- dma: Let st*_dma() take MemTxAttrs argument
- dma: Let ld*_dma() take MemTxAttrs argument
- dma: Let st*_dma() propagate MemTxResult
- dma: Let ld*_dma() propagate MemTxResult
- pci: Let st*_pci_dma() take MemTxAttrs argument
- pci: Let ld*_pci_dma() take MemTxAttrs argument
- pci: Let st*_pci_dma() propagate MemTxResult
- pci: Let ld*_pci_dma() propagate MemTxResult
- hw/audio/intel-hda: Do not ignore DMA overrun errors
- hw/audio/intel-hda: Restrict DMA engine to memories (not MMIO devices)
- tests/qtest/intel-hda-test: Add reproducer for issue #542

* Mon May 30 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-38
- acpi: fix QEMU crash when started with SLIC table
- tests: acpi: whitelist expected blobs before changing them
- tests: acpi: add SLIC table test
- tests: acpi: SLIC: update expected blobs
- hw/block/fdc: Prevent end-of-track overrun (CVE-2021-3507)
- tests/qtest/fdc-test: Add a regression test for CVE-2021-3507

* Mon May 30 2022 zhangziyang <zhangziyang1@huawei.com> - 2:6.2.0-37
- add qemu-system-x86_64, qemu-system-aarch64, qemu-system-arm rpm package build

* Thu May 26 2022 Jun Yang <jun.yang@suse.com> - 2:6.2.0-36
- Remove unnecessary dependency of kernel package

* Sat May 21 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-35
- hw/intc/arm_gicv3: Check for !MEMTX_OK instead of MEMTX_ERROR (CVE-2021-3750)
- softmmu/physmem: Simplify flatview_write and address_space_access_valid
- softmmu/physmem: Introduce MemTxAttrs::memory field and MEMTX_ACCESS_ERROR

* Tue May 10 2022 yezengruan <yezengruan@huawei.com> - 2:6.2.0-34
- display/qxl-render: fix race condition in qxl_cursor (CVE-2021-4207)
- ui/cursor: fix integer overflow in cursor_alloc (CVE-2021-4206)

* Wed Apr 27 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-33
- update the format of changelog

* Wed Apr 27 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-32
- vfio/pci: Ascend710 change to bar2 quirk

* Fri Apr 15 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-31
- vhost-vsock: detach the virqueue element in case of error (CVE-2022-26354)
- virtio-net: fix map leaking on error during receive (CVE-2022-26353)

* Wed Mar 30 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-30
- scsi-bus: fix incorrect call for blk_error_retry_reset_timeout()
- Revert "monitor: limit io error qmp event to at most once per 60s"

* Fri Mar 25 2022 Jinhua Cao <caojinhua1@huawei.com> - 6.2.0-29
- qemu-img create: 'cache' paramter only use for reg file image

* Thu Mar 24 2022 Yan Wang <wangyan122@huawei.com> - 6.2.0-28
- spec: add hw-usb-host rpm package

* Fri Mar 18 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-27
- coro: support live patch for libcare
- add patch for sw64 support

* Tue Mar 15 2022 jiangdawei <jiangdawei15@huawei.com> - 6.2.0-26
- cflags: add ftls-mode=initial-exec

* Tue Mar 15 2022 yezengruan <yezengruan@huawei.com> - 6.2.0-25
- sw_64: Add sw64 architecture support
- update BinDir

* Mon Mar 14 2022 jiangdawei <jiangdawei15@huawei.com> - 6.2.0-24
- qemu.spec: Add --enable-debug parameter to configure

* Thu Mar 03 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-23
- tools/virtiofsd: Add rseq syscall to the seccomp allowlist

* Thu Mar 03 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-23
- scsi-bus: fix unmatched object_unref()

* Sat Feb 26 2022 Yan Wang <wangyan122@huawei.com> - 6.2.0-22
- pl011-reset-read-FIFO-when-UARTTIMSC-0-UARTICR-0xfff.patch
- qcow2-fix-memory-leak-in-qcow2_read_extensions.patch
- scsi-disk-define-props-in-scsi_block_disk-to-avoid-m.patch
- pcie-Add-pcie-root-port-fast-plug-unplug-feature.patch
- pcie-Compat-with-devices-which-do-not-support-Link-W.patch

* Wed Feb 23 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-21
- acpi/madt: Factor out the building of MADT GICC struct
- hw/arm/virt: Assign virt_madt_cpu_entry to acpi_ged madt_cpu hook
- arm/virt/acpi: Factor out CPPC building from DSDT CPU aml
- acpi/cpu: Prepare build_cpus_aml for arm virt
- acpi/ged: Extend ACPI GED to support CPU hotplug
- arm/cpu: assign arm_get_arch_id handler to get_arch_id hook
- tests/acpi/bios-tables-test: Allow changes to virt/DSDT file
- arm/virt: Attach ACPI CPU hotplug support to virt
- tests/acpi/bios-table-test: Update expected virt/DSDT file
- arm/virt: Add CPU hotplug framework
- arm/virt: Add CPU topology support
- test/numa: Adjust aarch64 numa test
- hw/arm/virt: Factor out some CPU init codes to pre_plug hook
- hw/arm/boot: Add manually register and trigger of CPU reset
- arm/virt/gic: Construct irqs connection from create_gic
- intc/gicv3_common: Factor out arm_gicv3_common_cpu_realize
- intc/gicv3_cpuif: Factor out gicv3_init_one_cpuif
- intc/kvm_gicv3: Factor out kvm_arm_gicv3_cpu_realize
- hw/intc/gicv3: Add CPU hotplug realize hook
- accel/kvm: Add pre-park vCPU support
- intc/gicv3: Add pre-sizing capability to GICv3
- acpi/madt: Add pre-sizing capability to MADT GICC struct
- arm/virt: Add cpu_hotplug_enabled field
- arm/virt/acpi: Extend cpufreq to support max_cpus
- arm/virt: Pre-sizing MADT-GICC GICv3 and Pre-park KVM vCPU
- arm/virt: Start up CPU hot-plug and cold-plug

* Mon Feb 21 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-20
- i386/cpu: fix compile error in all target configure

* Mon Feb 21 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-19
- pl031: support rtc-timer property for pl031

* Mon Feb 21 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-19
- target/arm: Fix some compile errors

* Mon Feb 21 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-19
- Revert "qmp: add command to query used memslots of vhost-net and vhost-user"

* Thu Feb 17 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-19
- qapi/machine.json: Fix incorrect description for die-id
- tests/unit/test-smp-parse: Pass machine type as
- tests/unit/test-smp-parse: Split the 'generic' test in
- tests/unit/test-smp-parse: Add 'smp-with-dies' machine
- tests/unit/test-smp-parse: Add 'smp-generic-invalid'
- tests/unit/test-smp-parse: Add 'smp-generic-valid'
- tests/unit/test-smp-parse: Simplify pointer to compound
- tests/unit/test-smp-parse: Constify some pointer/struct
- hw/core: Rename smp_parse() ->
- qemu-options: Improve readability of SMP related Docs
- hw/core/machine: Introduce CPU cluster topology support
- tests/unit/test-smp-parse: Add testcases for CPU
- tests/unit/test-smp-parse: No need to explicitly zero
- tests/unit/test-smp-parse: Keep default MIN/MAX CPUs in
- hw/arm/virt: Support CPU cluster on ARM virt machine
- hw/arm/virt: Support cluster level in DT cpu-map
- hw/acpi/aml-build: Improve scalability of PPTT
- tests/acpi/bios-tables-test: Allow changes to virt/PPTT
- hw/acpi/aml-build: Support cluster level in PPTT
- tests/acpi/bios-table-test: Update expected virt/PPTT
- update BinDir
- softmmu/device_tree: Silence compiler warning with
- softmmu/device_tree: Remove redundant pointer
- hw/arm64: add vcpu cache info support
- update BinDir
- arm64: Add the cpufreq device to show cpufreq info to
- update BinDir

* Thu Feb 17 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-18
- bios-tables-test: Update expected q35/SSDT.dimmpxm file
- spec: add BinDir

* Tue Feb 15 2022 Liuxiangdong <liuxiangdong5@huawei.com> - 6.2.0-17
- feature: disable spice protocol

* Mon Feb 14 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-16
- log: Delete redudant qemu_log

* Mon Feb 14 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-15
- qemu-img: add qemu-img direct create

* Mon Feb 14 2022 eillon <yezhenyu2@huawei.com> - 6.2.0-15
- seabios: add check to avoid dereference NULL pointer

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-14
- bugfix: irq: Avoid covering object refcount of qemu_irq

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-13
- virtio-scsi: bugfix: fix qemu crash for hotplug scsi disk with dataplane
- virtio: net-tap: bugfix: del net client if net_init_tap_one failed
- virtio: bugfix: clean up callback when del virtqueue
- virtio-net: bugfix: do not delete netdev before virtio net
- virtio-net: fix max vring buf size when set ring num
- virtio: check descriptor numbers
- virtio: bugfix: add rcu_read_lock when vring_avail_idx is called
- virtio: print the guest virtio_net features that host does not support
- virtio: bugfix: check the value of caches before accessing it
- virtio-net: set the max of queue size to 4096
- virtio-net: update the default and max of rx/tx_queue_size
- vhost-user: add unregister_savevm when vhost-user cleanup
- qemu-img: block: dont blk_make_zero if discard_zeroes false
- vhost-user: Add support reconnect vhost-user socket
- vhost-user: Set the acked_features to vm's featrue
- vhost-user: add vhost_set_mem_table when vm load_setup at destination
- vhost-user: add separate memslot counter for vhost-user
- vhost-user: quit infinite loop while used memslots is more than the backend limit
- qmp: add command to query used memslots of vhost-net and vhost-user
- vhost-user-scsi: add support for SPDK hot upgrade
- i6300esb watchdog: bugfix: Add a runstate transition

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-13
- bugfix: fix some illegal memory access and memory leak
- bugfix: fix possible memory leak
- bugfix: fix eventfds may double free when vm_id reused in ivshmem
- block/mirror: fix file-system went to read-only after block-mirror
- bugfix: fix mmio information leak and ehci vm escape 0-day vulnerability
- target-i386: Fix the RES memory inc which caused by the coroutine created

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-13
- log: Add log at boot & cpu init for aarch64
- feature: Add log for each modules
- feature: Add logs for vm start and destroy

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-12
- linux-headers: update against 5.10 and manual clear vfio dirty log series
- vfio: Maintain DMA mapping range for the container
- vfio/migration: Add support for manual clear vfio dirty log
- update-linux-headers: Import iommu.h
- vfio.h and iommu.h header update against 5.10
- memory: Add new fields in IOTLBEntry
- hw/arm/smmuv3: Improve stage1 ASID invalidation
- hw/arm/smmu-common: Allow domain invalidation for NH_ALL/NSNH_ALL
- memory: Add IOMMU_ATTR_VFIO_NESTED IOMMU memory region attribute
- memory: Add IOMMU_ATTR_MSI_TRANSLATE IOMMU memory region attribute
- memory: Introduce IOMMU Memory Region inject_faults API
- iommu: Introduce generic header
- pci: introduce PCIPASIDOps to PCIDevice
- vfio: Force nested if iommu requires it
- vfio: Introduce hostwin_from_range helper
- vfio: Introduce helpers to DMA map/unmap a RAM section
- vfio: Set up nested stage mappings
- vfio: Pass stage 1 MSI bindings to the host
- vfio: Helper to get IRQ info including capabilities
- vfio/pci: Register handler for iommu fault
- vfio/pci: Set up the DMA FAULT region
- vfio/pci: Implement the DMA fault handler
- hw/arm/smmuv3: Advertise MSI_TRANSLATE attribute
- hw/arm/smmuv3: Store the PASID table GPA in the translation config
- hw/arm/smmuv3: Fill the IOTLBEntry arch_id on NH_VA invalidation
- hw/arm/smmuv3: Fill the IOTLBEntry leaf field on NH_VA invalidation
- hw/arm/smmuv3: Pass stage 1 configurations to the host
- hw/arm/smmuv3: Implement fault injection
- hw/arm/smmuv3: Allow MAP notifiers
- pci: Add return_page_response pci ops
- vfio/pci: Implement return_page_response page response callback
- vfio/common: Avoid unmap ram section at vfio_listener_region_del() in nested mode
- vfio: Introduce helpers to mark dirty pages of a RAM section
- vfio: Add vfio_prereg_listener_log_sync in nested stage
- vfio: Add vfio_prereg_listener_log_clear to re-enable mark dirty pages
- vfio: Add vfio_prereg_listener_global_log_start/stop in nested stage
- hw/arm/smmuv3: Post-load stage 1 configurations to the host
- vfio/common: Fix incorrect address alignment in vfio_dma_map_ram_section
- vfio/common: Add address alignment check in vfio_listener_region_del

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-11
- log: Add some logs on VM runtime path
- qdev/monitors: Fix reundant error_setg of qdev_add_device
- bios-tables-test: Allow changes to q35/SSDT.dimmpxm file
- smbios: Add missing member of type 4 for smbios 3.0
- net: eepro100: validate various address valuesi(CVE-2021-20255)
- pci: check bus pointer before dereference
- ide: ahci: add check to avoid null dereference (CVE-2019-12067)
- tap: return err when tap TUNGETIFF fail
- xhci: check reg to avoid OOB read
- monitor: Discard BLOCK_IO_ERROR event when VM rebooted
- monitor: limit io error qmp event to at most once per 60s

* Sat Feb 12 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-11
- util/log: add CONFIG_DISABLE_QEMU_LOG macro

* Sat Feb 12 2022 Yan Wang <wangyan122@huawei.com> - 6.2.0-11
- ipxe: IPv6 add support for IPv6 protocol
- u-boot: Use post increment only in inffast.c

* Sat Feb 12 2022 jiangdongxu <jiangdongxu1@huawei.com> - 6.2.0-10
- seabios: convert value of be16_to_cpu to u64 before shifting
- seabios: do not give back high ram
- seabios: fix memory leak when pci check
- seabios: drop yield() in smp_setup()
- seabios: increase the seabios minibiostable
- seabios: increase the seabios high mem zone size

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-9
- hw/net/rocker: fix security vulnerability
- tests: Disable filemonitor testcase

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-8
- hw/usb: reduce the vpcu cost of UHCI when VNC disconnect

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-8
- freeclock: add qmp command to get time offset of vm in seconds
- freeclock: set rtc_date_diff for arm
- freeclock: set rtc_date_diff for X86

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-8
- target/arm: convert isar regs to array
- target/arm: parse cpu feature related options
- target/arm: register CPU features for property
- target/arm: Allow ID registers to synchronize to KVM
- target/arm: introduce CPU feature dependency mechanism
- target/arm: introduce KVM_CAP_ARM_CPU_FEATURE
- target/arm: Add CPU features to query-cpu-model-expansion
- target/arm: Add more CPU features
- target/arm: ignore evtstrm and cpuid CPU features
- target/arm: only set ID_PFR1_EL1.GIC for AArch32 guest
- target/arm: Fix write redundant values to kvm
- target/arm: clear EL2 and EL3 only when kvm is not enabled
- target/arm: Update the ID registers of Kunpeng-920

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-8
- i386: cache passthrough: Update Intel CPUID4.EAX[25:14] based on vCPU topo
- i386: cache passthrough: Update AMD 8000_001D.EAX[25:14] based on vCPU topo

* Fri Feb 11 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-8
- nbd/server.c: fix invalid read after client was already free
- qemu-nbd: make native as the default aio mode
- qemu-nbd: set timeout to qemu-nbd socket
- qemu-pr: fixed ioctl failed for multipath disk
- block: enable cache mode of empty cdrom
- block: disallow block jobs when there is a BDRV_O_INACTIVE flag
- scsi: cdrom: Fix crash after remote cdrom detached
- block: bugfix: disable process AIO when attach scsi disk
- block: bugfix: Don't pause vm when NOSPACE EIO happened
- scsi: bugfix: fix division by zero

* Fri Feb 11 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-8
- migration: skip cache_drop for bios bootloader and
- ps2: fix oob in ps2 kbd
- Currently, while kvm and qemu can not handle some kvm
- cpu/features: fix bug for memory leakage
- monitor/qmp: drop inflight rsp if qmp client broken
- oslib-posix: optimise vm startup time for 1G hugepage

* Fri Feb 11 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-7
- scsi-bus: Refactor the code that retries requests
- scsi-disk: Add support for retry on errors
- block-backend: Stop retrying when draining
- block: Add sanity check when setting retry parameters

* Fri Feb 11 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-7
- vfio/pci: Ascend310 need 4Bytes quirk in bar4
- vfio/pci: Ascend710 need 4Bytes quirk in bar0
- vfio/pci: Ascend910 need 4Bytes quirk in bar0

* Fri Feb 11 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-7
- hugepages: hugepages files maybe leftover
- Patch0024: target-i386: Modify the VM's physical bits value set

* Fri Feb 11 2022 Yan Wang <wangyan122@huawei.com> - 6.2.0-7
- log: disable qemu_log function for "make check V=1"

* Fri Feb 11 2022 Yan Wang <wangyan122@huawei.com> - 6.2.0-6
- chardev/baum: disable unused brlapi

* Fri Feb 11 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-5
- Revert "cpu: parse +/- feature to avoid failure"
- Revert "cpu: add Cortex-A72 processor kvm target support"
- cpu: add Cortex-A72 processor kvm target support

* Thu Feb 10 2022 imxcc <xingchaochao@huawei.com> - 6.2.0-4
- qapi/block-core: Add retry option for error action
- qapi/block-core: Add retry option for error action
- block-backend: Introduce retry timer
- block-backend: Add device specific retry callback
- block-backend: Enable retry action on errors
- block-backend: Add timeout support for retry
- block: Add error retry param setting
- virtio_blk: Add support for retry on errors
- vhost: cancel migration when vhost-user restarted
- migration: Add multi-thread compress method
- migration: Refactoring multi-thread compress migration
- migration: Add multi-thread compress ops
- migration: Add zstd support in multi-thread compression
- migration: Add compress_level sanity check
- doc: Update multi-thread compression doc

* Wed Feb 09 2022 Chen Qun <kuhn.chenqun@huawei.com> - 6.2.0-3
- cpu: parse +/- feature to avoid failure
- cpu: add Kunpeng-920 cpu support
- cpu: add Cortex-A72 processor kvm target support
- add Phytium's CPU models: FT-2000+ and Tengyun-S2500.

* Tue Feb 8 2022 Xiangdong Liu <liuxiangdong5@huawei.com> - 6.2.0-2
- net/dump.c: Suppress spurious compiler warning

* Thu Jan 27 2022 Xiangdong Liu <liuxiangdong5@huawei.com> - 6.2.0-1
- Package init
